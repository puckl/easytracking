<?php

/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\EasyTracking\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\DatabaseProvider;

class Events
{
    protected static function _moduleID()
    {
        return 'ecs_easytracking';
    }

    public static function addSql()
    {
        $aSql[] = '';

        foreach ($aSql as $sSql) {
            try {
                DatabaseProvider::getDb()->execute($sSql);
            } catch (\Exception $oException) {
            }
        }
        return true;
    }

    public static function dropSql()
    {
        $aSql[] = 'DELETE FROM oxtplblocks WHERE OXMODULE= ' . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote(self::_moduleID()) . ';';

        foreach ($aSql as $sSql) {
            try {
                DatabaseProvider::getDb()->execute($sSql);
            } catch (\Exception $oException) {
            }
        }
        return true;
    }

    public static function clearTmp($sClearFolderPath = '')
    {
        $sFolderPath = self::_getFolderToClear($sClearFolderPath);
        $hDirHandler = opendir($sFolderPath);
        if (!empty($hDirHandler)) {
            while (false !== ($sFileName = readdir($hDirHandler))) {
                $sFilePath = $sFolderPath . DIRECTORY_SEPARATOR . $sFileName;
                self::_clear($sFileName, $sFilePath);
            }
            closedir($hDirHandler);
        }
        return true;
    }

    protected static function _getFolderToClear($sClearFolderPath = '')
    {
        $sTempFolderPath = (string) Registry::getConfig()->getConfigParam('sCompileDir');
        if (!empty($sClearFolderPath) and (strpos($sClearFolderPath, $sTempFolderPath) !== false)) {
            $sFolderPath = $sClearFolderPath;
        } else {
            $sFolderPath = $sTempFolderPath;
        }
        return $sFolderPath;
    }

    protected static function _clear($sFileName, $sFilePath)
    {
        if (!in_array($sFileName, ['.', '..', '.gitkeep', '.htaccess'])) {
            if (is_file($sFilePath)) {
                @unlink($sFilePath);
            } else {
                self::clearTmp($sFilePath);
            }
        }
    }

    public static function regenViews()
    {
        $oMD = oxNew(DbMetaDataHandler::class);
        $oMD->updateViews();
    }

    public static function onActivate()
    {
        //self::addSql();
        self::clearTmp();
        self::regenViews();
    }

    public static function onDeactivate()
    {
        self::dropSql();
        self::clearTmp();
        self::regenViews();
    }
}
