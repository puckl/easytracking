eComStyle.de EasyTracking
================================

Einfachste Integration des Trackingcodes inkl. eCommerce-Tracking für Matomo- und/oder Google Analytics (inkl.
Remarketing-Tag und Adwords Conversion Tracking) mit einen einzigen Modul!


Composer name
---------------
ecs/easytracking


Geeignet für Shopversion
-------------------------
OXID eShop CE/PE 6  


Vorbereitung der Modulinstallation
-----------------------------------

Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgende Befehle aus (bitte jede Zeile einzeln ausführen):

    md vendor/zip

    composer config repo.meinzip artifact ./vendor/zip


Bitte beachten:
Die o.g. Vorbereitung der Installation muss nur einmal ausgeführt werden.
Bei weiteren Modulinstallation kann direkt mit der Modulinstallation begonnen werden.


Modulinstallation
------------------

1. Loggen Sie sich via FTP auf dem Server ein und kopieren Sie die ZIP-Datei (das eComStyle.de-Modul) unverändert in den Ordner vendor/zip.

2. Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgenden Befehl aus:

    composer require ecs/easytracking

Die dann folgenden Abfragen in der Console können einfach mit ENTER quittiert werden.

3. Loggen Sie sich anschließend in Ihren Shop-Admin ein und aktivieren das neue Modul unter Erweiterungen/Module.



Modulupdate
------------

1. Loggen Sie sich via FTP auf dem Server ein und kopieren Sie die ZIP-Datei (das eComStyle.de-Modul) unverändert in den Ordner vendor/zip.
Eine evtl. bereits vorhandene, gleichnamige ZIP-Datei (zB. eien alte Modulversion) kann einfach gelöscht werden.

2. Erstellen Sie via SSH-Client eine Verbindung mit dem Server, auf dem Ihr OXID eShop liegt.
Wechseln Sie in das OXID-Projektverzeichnis, in dem sich die Datei composer.json sowie die source- und vendor-Ordner befinden.
Führen Sie dort folgenden Befehl aus:

    composer update

Bei den dann folgenden Abfragen in der Console sollte sie für das upgedatete Modul die Frage "Do you want to overwrite them? (y/N)" mit "y" bestätigen.
Nur dann wird die alte Modulversion im Ordner source/modules/ecs durch die aktuelle Version ersetzt.

3. Loggen Sie sich anschließend in Ihren Shop-Admin ein, deaktivieren Sie das Modul einmal und aktivieren es dann wieder.
Dadurch werden ggf. neue Moduleinstellungen aktiviert, neue Datenbankfelder angelegt und der TMP geleert.



Moduleinstellungen:
-------------------

### Cookie-Banner

* Cookie-Banner aktivieren.
Bevor ein Tracking-Script geladen und ein Tracking-Cookie gesetzt wird, wird in einen Popup um Zustimmung gebeten.
Bitte sehen Sie diese Funktion als Vorschlag an, auf keinen Fall garantiert der Modulhersteller, dass es sich um eine rechtssichere Lösung handelt.
Für die Rechtssicherheit seines Shops ist alleine der Shopbetreiber verantwortlich.


### Matomo Webanalyse

* Matomo aktivieren.
* Die URL der Matomo-Installation hier eintragen. Kein / am Ende! (zB. https://matomo.de, https://shop.de/matomo)
* Die Matomo Seiten ID hier eintragen.
* Bei eingeloggten Usern die Kundennr. zu Matomo übermitteln
* Cookies aktivieren (hilft u.a., wiederkehrende Besucher zu erkennen, kann aber wegen dem Datenschutz problematisch sein).
* token_auth, zu finden in den Matomo-Einstellungen unter Persönlich/Einstellungen/API Authentifizierungs-Token.


### Google Analytics:
Die Tracking-ID finden Sie in Ihrem Google-Analytics-Konto unter Verwalten > Property > Tracking Informationen > Tracking-Code.
Sie sollten auch das eCommerce-Tracking aktivieren unter Verwalten > Datenansicht > E-Commerce-Einstellungen.

### Google Adwords und Remarketing:
Die erforderlichen ID´s finden Sie in ihren Adwords-Konto.

### Opt-out: Besucher können das Tracking deaktivieren:
In Piwik finden Sie unter Einstellungen > Privatsphäre > Piwik-Deaktivierung den für Ihre Website passenden Codeschnipsel. Diesen können Sie zB. in Ihre Datenschutzerklärung integrieren.

Für Google Analytics ist eine Opt-out-Funktion im Modul integriert. Sie können folgenden Opt-Out-Link auf Ihrer Website zB. in der Datenschutzerklärung anbieten:

    <a onclick="alert('Google Analytics wurde deaktiviert');"
    href="javascript:gaOptout()">Google Analytics deaktivieren</a>


Für die Rechtssicherheit übernehmen wir keine Gewähr!


Lizenzinformationen
-------------------
Please retain this copyright header in all versions of the software

Copyright (C) Josef A. Puckl | eComStyle.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see {http://www.gnu.org/licenses/}.



Sonstige Wünsche
----------------
Sehr gerne sind wir Ihnen bei allen Aufgaben rund um Ihren Onlineshop behilflich!
Fragen Sie bitte jederzeit einfach an: <https://ecomstyle.de/kontakt/>