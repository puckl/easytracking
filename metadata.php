<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion = '2.0';
$aModule = [
    'id'            => 'ecs_easytracking',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>EasyTracking</i>',
    'description'   => 'Einfache Matomo- und Google Analytics-Integration in den OXID eShop 6.',
    'version'       => '3.1.2',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend'        => [
    ],
    'controllers' => [
        'ecs_easytracking' => Ecs\EasyTracking\Controller\Admin\EasyTracking::class,
    ],
    'templates' => [
        'ecs_easytracking.tpl' => 'ecs/EasyTracking/views/admin/tpl/ecs_easytracking.tpl'
    ],
    'blocks' => [
        ['template' => 'layout/base.tpl',   'block' => 'base_js',     'file' => '/views/blocks/easytracking.tpl'],
        ['template' => 'layout/base.tpl',   'block' => 'base_style',  'file' => '/views/blocks/easytracking_head.tpl'],
        ['template' => 'layout/footer.tpl', 'block' => 'footer_main', 'file' => '/views/blocks/easytracking_footer.tpl'],
    ],
    'settings' => [
        ['group' => 'ecs_ban', 'name' => 'ecs_conban',    'type' => 'bool', 'value' => true],
        ['group' => 'ecs_ban', 'name' => 'ecs_backdr',    'type' => 'bool', 'value' => true],
        ['group' => 'ecs_ban', 'name' => 'ecs_banbottom', 'type' => 'bool', 'value' => false],

        ['group' => 'ecs_piwik', 'name' => 'ecs_usepiwik',          'type' => 'bool', 'value' => false],
        ['group' => 'ecs_piwik', 'name' => 'ecs_piwikurl',          'type' => 'str',  'value' => 'https://piwikdomain.de'],
        ['group' => 'ecs_piwik', 'name' => 'ecs_piwikid',           'type' => 'str',  'value' => '1'],
        ['group' => 'ecs_piwik', 'name' => 'ecs_piwikuserid',       'type' => 'bool', 'value' => false],
        ['group' => 'ecs_piwik', 'name' => 'ecs_piwikcookie',       'type' => 'bool', 'value' => false],
        ['group' => 'ecs_piwik', 'name' => 'ecs_piwikadmintoken',   'type' => 'str',  'value' => 'abcdefghijklmnopqrstuvwyxz'],

        ['group' => 'ecs_ga', 'name' => 'ecs_usega',        'type' => 'bool', 'value' => false],
        ['group' => 'ecs_ga', 'name' => 'ecs_usegacookies', 'type' => 'bool', 'value' => false],
        ['group' => 'ecs_ga', 'name' => 'ecs_gaid',         'type' => 'str',  'value' => 'UA-XXXX-Y'],
        ['group' => 'ecs_ga', 'name' => 'ecs_useadw',       'type' => 'bool', 'value' => false],
        ['group' => 'ecs_ga', 'name' => 'ecs_adwid',        'type' => 'str',  'value' => '123456789'],
        ['group' => 'ecs_ga', 'name' => 'ecs_adwlab',       'type' => 'str',  'value' => 'ConversionLabel'],
        ['group' => 'ecs_ga', 'name' => 'ecs_userema',      'type' => 'bool', 'value' => false],
        ['group' => 'ecs_ga', 'name' => 'ecs_remaid',       'type' => 'str',  'value' => '123456789'],
        ['group' => 'ecs_ga', 'name' => 'ecs_remalab',      'type' => 'str',  'value' => 'ConversionLabel'],
    ],
    'events' => [
        'onActivate'   => 'Ecs\EasyTracking\Core\Events::onActivate',
        'onDeactivate' => 'Ecs\EasyTracking\Core\Events::onDeactivate',
    ],
];
