<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$garemarketing = OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('ecs_userema');
$gaadwords     = OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('ecs_useadw');

$moreinfotext =
'<p class="text-left">
    <strong>Notwendige Cookies</strong><br>
    Diese Cookies sind für den Betrieb der Seite unbedingt notwendig.<br>
    Das Shopsystem speichert in diesen Cookies z.B. den Inhalt Ihres Warenkorbs oder Ihre Spracheinstellung.<br>
    Notwendige Cookies können nicht deaktiviert werden, da unser Shop ansonsten nicht funktionieren würde.
</p>';

$moreinfotext .=
'<p class="text-left">
    <strong>Statistik</strong><br>
    Um unser Artikelangebot weiter zu verbessern, erfassen wir anonymisierte Daten für Statistiken und Analysen.<br>
    Aufgrund dieser Statistiken können wir unsere Angebot für Sie optimieren.  
</p>';

if ($garemarketing || $gaadwords){
    $moreinfotext .=
    '<p class="text-left">
        <strong>Personalisierung</strong><br>
        Wir können Ihnen personalisierte Inhalte, passend zu Ihren Interessen anzeigen.<br>
        Somit können wir Ihnen Angebote präsentieren, die für Sie besonders relevant sind.
    </p>';
}

$topinfotext =
    '<p class="text-left">
    Wir verwenden Cookies, um Ihnen ein optimales Einkaufserlebnis zu bieten.<br>
    Einige Cookies sind technisch notwendig, andere dienen zu anonymen Statistikzwecken.<br>
    Entscheiden Sie bitte selbst, welche Cookies Sie akzeptieren.
</p>';


$sLangName = "Deutsch";
$aLang     = [

    'charset'                   => 'UTF-8',

    'ecs_et_topinfo'            => $topinfotext,
    'ecs_et_moreinfotxt'        => $moreinfotext,

    'ecs_et_cookieset'          => 'Cookie-Einstellungen',
    'ecs_et_allowtech'          => 'Notwendige Cookies erlauben',
    'ecs_et_allowstat'          => 'Statistik erlauben',
    'ecs_et_allowcomf'          => 'Komfort erlauben',
    'ecs_et_allowpers'          => 'Personalisierung erlauben',
    'ecs_et_selectchecked'      => 'Auswahl bestätigen',
    'ecs_et_selectall'          => 'Alle auswählen und bestätigen',
    'ecs_et_moreinfo'           => 'Weitere Infos'

];
