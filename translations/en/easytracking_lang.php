<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$garemarketing = OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('ecs_userema');
$gaadwords     = OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam('ecs_useadw');

$moreinfotext =
'<p class="text-left">
    <strong>Necessary Cookies</strong><br>
     These cookies are essential for the operation of the site.<br>
     The shop system stores in these cookies e.g. the contents of your shopping cart or your language setting.<br>
     Necessary cookies can not be deactivated, as our shop would not work otherwise.
</p>';

$moreinfotext .=
'<p class="text-left">
    <strong>Statistics</strong><br>
    To further improve our product offering, we collect anonymized data for statistics and analysis.
</p>';

if ($garemarketing || $gaadwords){
    $moreinfotext .=
    '<p class="text-left">
        <strong>Personalization</strong><br>
        We can display personalized content that suits your interests.<br>
        Thus, we can present offers that are particularly relevant to you.
    </p>';
}

$topinfotext =
    '<p class="text-left">
     We use cookies to give you the best shopping experience.<br>
     Some cookies are technically necessary, others are for anonymous statistical purposes.<br>
     Please decide for yourself which cookies you accept.
</p>';

$sLangName = "English";
$aLang     = [

    'charset'                   => 'UTF-8',

    'ecs_et_topinfo'            => $topinfotext,
    'ecs_et_moreinfotxt'        => $moreinfotext,

    'ecs_et_cookieset'          => 'Cookie-Settings',
    'ecs_et_allowtech'          => 'Allow necessary cookies',
    'ecs_et_allowstat'          => 'Allow statistics',
    'ecs_et_allowcomf'          => 'Allow comfort',
    'ecs_et_allowpers'          => 'Allow personalization',
    'ecs_et_selectchecked'      => 'Confirm selection',
    'ecs_et_selectall'          => 'Select all and confirm',
    'ecs_et_moreinfo'           => 'More Info'

];
