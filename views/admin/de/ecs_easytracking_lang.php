<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sLangName = 'Deutsch';
$aLang     = [

    'charset'                           => 'UTF-8',

    'ecsmenu'                           => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'ecstools_menu'                     => 'Tools',
    'ecs_easytracking'                  => 'Matomo Tracking',

    'SHOP_MODULE_GROUP_ecs_main'        => 'Grundeinstellungen',
    'SHOP_MODULE_GROUP_ecs_piwik'       => 'Matomo Webanalyse',
    'SHOP_MODULE_GROUP_ecs_ban'         => 'Cookie-Banner',

    'SHOP_MODULE_ecs_conban'            => 'Cookie-Banner aktivieren.',
    'SHOP_MODULE_ecs_backdr'            => 'Grauer Schleier (Hintergrund) bei Cookie-Banner.',
    'SHOP_MODULE_ecs_banbottom'         => 'Cookie-Banner am unteren Seitenrand (Styleblock einbinden).',
    'SHOP_MODULE_ecs_piwikurl'          => 'Die URL der Matomo-Installation hier eintragen. Kein <b>/</b> am Ende! (zB. https://matomo.de, https://shop.de/matomo)',
    'SHOP_MODULE_ecs_piwikid'           => 'Die Matomo Seiten ID hier eintragen.',
    'SHOP_MODULE_ecs_usepiwik'          => 'Matomo aktivieren.',
    'SHOP_MODULE_ecs_piwikuserid'       => 'Bei eingeloggten Usern die Kundennr. zu Matomo übermitteln',
    'SHOP_MODULE_ecs_piwikcookie'       => 'Cookies aktivieren (hilft u.a., wiederkehrende Besucher zu erkennen, kann aber wegen dem Datenschutz problematisch sein).',
    'SHOP_MODULE_ecs_piwikadmintoken'   => 'token_auth, zu finden in den Matomo-Einstellungen unter Persönlich/Einstellungen/API Authentifizierungs-Token.',

    'SHOP_MODULE_GROUP_ecs_ga'          => 'Google Analytics',
    'SHOP_MODULE_ecs_gaid'              => 'Die Google Analytics Tracking-ID hier eintragen.',
    'SHOP_MODULE_ecs_usegacookies'      => 'Cookies für Google Analytics aktivieren (hilft u.a., wiederkehrende Besucher zu erkennen, kann aber wegen dem Datenschutz problematisch sein).',
    'SHOP_MODULE_ecs_usega'             => 'Google Analytics aktivieren.',
    'SHOP_MODULE_ecs_useadw'            => 'Google Adwords aktivieren.',
    'SHOP_MODULE_ecs_adwlab'            => 'Das Google Adwords-Label hier eintragen.',
    'SHOP_MODULE_ecs_adwid'             => 'Die Google Adwords-ID hier eintragen.',
    'SHOP_MODULE_ecs_remaid'            => 'Die Google Remarketing-ID hier eintragen.',
    'SHOP_MODULE_ecs_userema'           => 'Google Remarketing aktivieren.',
    'SHOP_MODULE_ecs_remalab'           => 'Google Remarketing-Label hier eintragen.',
];
