[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
*}]
[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box=" "}]
[{if !isset($oConfig)}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]
[{if $oConfig->getConfigParam('ecs_piwikurl') and $oConfig->getConfigParam('ecs_piwikid') and $oConfig->getConfigParam('ecs_piwikadmintoken') }]
    [{assign var="urlpiwik"     value=$oConfig->getConfigParam('ecs_piwikurl')}]
    [{assign var="PiwikSiteID"  value=$oConfig->getConfigParam('ecs_piwikid')}]
    [{assign var="PiwikAuthKey" value=$oConfig->getConfigParam('ecs_piwikadmintoken')}]
    <div class="piwikiframe">
        <iframe src="[{$urlpiwik}]/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=[{$PiwikSiteID}]&period=week&date=yesterday&token_auth=[{$PiwikAuthKey}]" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="880 px"></iframe>
    </div>
[{else}]
    <p>Piwik Tracking-Ansicht deaktiviert da Moduleinstellungen unvollst&auml;ndig.</p>
[{/if}]

<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous">
</script>
<script>
    $( document ).ready(function() {
         var windowHeight = $(window ).innerHeight() -20;
        $('.piwikiframe iframe').attr('height', windowHeight + 'px;');
    });
</script>