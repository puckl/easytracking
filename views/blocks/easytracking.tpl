[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{$smarty.block.parent}]
[{if !isset($oConfig)}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]
[{assign var="cookie" value=","|explode:$smarty.cookies.consent}][{*stat,pers,comf*}]
[{capture append="oxidBlock_pageScript"}]
    [{if !$oConfig->getConfigParam('ecs_conban') || $cookie[1]}]
        [{if $oConfig->getConfigParam('ecs_useadw')}]
            [{include file=$oViewConf->getModulePath('ecs/EasyTracking',"views/blocks/inc/easytracking_google_adwords.tpl")}]
        [{/if}]
        [{if $oConfig->getConfigParam('ecs_userema')}]
            [{include file=$oViewConf->getModulePath('ecs/EasyTracking',"views/blocks/inc/easytracking_google_remarketing.tpl")}]
        [{/if}]
    [{/if}]
    [{if !$oConfig->getConfigParam('ecs_conban') || $cookie[0]}]
        [{if $oConfig->getConfigParam('ecs_usepiwik')}]
            [{include file=$oViewConf->getModulePath('ecs/EasyTracking',"views/blocks/inc/easytracking_piwik.tpl")}]
        [{/if}]
    [{/if}]
[{/capture}]
