[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{$smarty.block.parent}]
[{if !isset($oConfig)}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]
[{if $oConfig->getConfigParam('ecs_conban') && !$smarty.cookies.consent}]
    [{if $oConfig->getConfigParam('ecs_userema') || $oConfig->getConfigParam('ecs_useadw')}]
        [{assign var="pers" value=true}]
    [{/if}]
    [{if $oViewConf->getActiveTheme()|strpos:"flow" !== false }][{strip}]
        <style type="text/css">
            #etrpop .mt-2{margin-top:1rem!important;}
            #etrpop .ml-2{margin-left:1rem!important;}
            #etrpop .mr-2{margin-right:1rem!important;}
            #etrpop .checkbox-inline+.checkbox-inline{margin-left:0px!important;}
            #etrpop .checkbox-inline{margin-right:10px;}
        </style>
    [{/strip}][{/if}]
    [{if $oConfig->getConfigParam('ecs_banbottom')}][{strip}]
        <style type="text/css">
            #etrpop .modal-lg{position:fixed;bottom:0;width:100%;max-width:100%;margin:0;}
            #etrpop .modal-content{border-radius:0;}
            body.modal-open{overflow:auto;}
            #etrpop{height:unset;top:unset;}
            #etrpop .modal-header,#etrpop .modal-body{max-width:1190px;width:100%;margin:auto;}
        </style>
    [{/strip}][{/if}]
    [{oxscript add="$('#etrpop').modal('show');"}]
    <div class="modal" id="etrpop" data-keyboard="false" data-backdrop="[{if $oConfig->getConfigParam('ecs_backdr')}]static[{else}]false[{/if}]">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-left">
                    <h2 class="modal-title">[{oxmultilang ident="ecs_et_cookieset"}]</h2>
                </div>
                <div class="modal-body">
                    [{oxmultilang ident="ecs_et_topinfo"}]
                    <p class="text-left">
                        <div class="form-check form-check-inline checkbox-inline">
                            <input class="form-check-input" type="checkbox" value="1" id="chk_tech" checked disabled>
                            <label class="form-check-label" for="chk_tech">
                                [{oxmultilang ident="ecs_et_allowtech"}]
                            </label>
                        </div>
                        <div class="form-check form-check-inline checkbox-inline">
                            <input class="form-check-input" type="checkbox" value="1" id="chk_stat">
                            <label class="form-check-label" for="chk_stat">
                                [{oxmultilang ident="ecs_et_allowstat"}]
                            </label>
                        </div>
                        [{if $pers}]
                            <div class="form-check form-check-inline checkbox-inline">
                                <input class="form-check-input" type="checkbox" value="1" id="chk_pers">
                                <label class="form-check-label" for="chk_pers">
                                    [{oxmultilang ident="ecs_et_allowpers"}]
                                </label>
                            </div>
                        [{/if}]
                    </p>
                    <div class="clearfix"></div>
                    <p class="text-left">
                        <a href="#" id="et-moreinfobtn" class="text-link" onclick="addCollapseShow()">
                            [{oxmultilang ident="ecs_et_moreinfo"}]&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                        </a>
                        <div class="collapse" id="cookiemoreinfo">
                            [{oxmultilang ident="ecs_et_moreinfotxt"}]
                        </div>
                    </p>
                    <hr>
                    <div class="text-left mb-2 mb-lg-0">
                        [{oxifcontent ident="oxsecurityinfo" object="oCont_secu"}]
                            <a id="securitypop" class="et-cmsbtns mr-2" rel="nofollow" href="[{$oCont_secu->getLink()}]" onclick="window.open('[{$oCont_secu->getLink()|oxaddparams:"plain=1"}]', 'securityinfo_popup', 'resizable=yes,status=no,scrollbars=yes,menubar=no,width=700,height=500');return false;">[{$oCont_secu->oxcontents__oxtitle->value}]</a>
                        [{/oxifcontent}]
                        [{oxifcontent ident="oximpressum" object="oCont_imprint"}]
                            <a id="imprintpop" class="et-cmsbtns" rel="nofollow" href="[{$oCont_imprint->getLink()}]" onclick="window.open('[{$oCont_imprint->getLink()|oxaddparams:"plain=1"}]', 'imprint_popup', 'resizable=yes,status=no,scrollbars=yes,menubar=no,width=700,height=500');return false;">[{$oCont_imprint->oxcontents__oxtitle->value}]</a>
                        [{/oxifcontent}]
                    </div>
                    <div class="text-right text-lg-right">
                        <a rel="nofollow" href="" class="mt-2 btn btn-outline-success btn-default" onclick="setConsentSelect()">[{oxmultilang ident="ecs_et_selectchecked"}]</a>
                        <a rel="nofollow" href="" class="mt-2 ml-2 btn btn-success" onclick="setConsentAll()">[{oxmultilang ident="ecs_et_selectall"}]</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    [{capture assign=easyTrackingCookies}][{strip}]
        function setConsentSelect() {
            stat_value = pers_value = comf_value = 0;
            if($('#chk_stat').is(':checked')){
                stat_value = 1;
            }
            [{if $pers}]
                if($('#chk_pers').is(':checked')){
                    pers_value = 1;
                }
            [{/if}]
            cvalue = stat_value+','+pers_value+','+comf_value;
            setCookie('consent',cvalue, '365');
            $('#etrpop').modal('hide');
        }
        function setConsentAll() {
            $(':checkbox').prop('checked', true);
            cvalue = '1,1,1';
            setCookie('consent', cvalue, '365');
            setTimeout("$('#etrpop').modal('hide')", 3000);
        }
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        function addCollapseShow(){
            $('#cookiemoreinfo').toggleClass('show');
            if($('#cookiemoreinfo').hasClass('show')){
                 $('#et-moreinfobtn i').attr('style','transform: rotate(180deg)');
            }else{
                $('#et-moreinfobtn i').removeAttr('style');
            }
        }
    [{/strip}][{/capture}]
    [{oxscript add=$easyTrackingCookies}]
[{/if}]
