[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{assign var="oConf"    value=$oViewConf->getConfig()}]
[{assign var="idga"     value=$oConf->getConfigParam('ecs_gaid')}]
[{assign var="iphash"   value=$smarty.server.REMOTE_ADDR|md5}]
<!-- Google Analytics eCommerce Tracking by https://eComStyle.de 4 OXID eShop 6 -->
<script type="text/javascript">
    [{*Set to the same value as the web property used on the site *}]
    var gaProperty = '[{$idga}]';
    [{*Disable tracking if the opt-out cookie exists.*}]
    var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
      window[disableStr] = true;
    }
    [{*Opt-out function*}]
    function gaOptout() {
      document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
      window[disableStr] = true;
    }
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      [{if $oConf->getConfigParam('ecs_usegacookies')}]
        ga('create', '[{$idga}]', 'auto');
      [{else}]
        ga('create', '[{$idga}]', {'storage':'none','clientId':'[{$iphash}]'});
      [{/if}]
      ga('set', 'anonymizeIp', true);[{*IP-unkenntlich machen *}]
      [{if $oConf->getConfigParam('ecs_userema')}]
            ga('require', 'displayfeatures');
      [{/if}]
      ga('send', 'pageview');
    [{* **********************************************************************************Ecommerce Analytics******************************* *}]
    [{if $oView->getClassName() == "thankyou"}]
        [{assign var="order" value=$oView->getOrder()}]
        ga('require', 'ecommerce', 'ecommerce.js');
        ga('ecommerce:addTransaction', {
          'id': '[{$order->oxorder__oxordernr}]',[{*Transaction ID. Required.*}]
          'affiliation': '[{$oxcmp_shop->oxshops__oxname->value}]',[{*Affiliation or store name.*}]
          'revenue': '[{$order->oxorder__oxtotalordersum}]',[{*Grand Total.*}]
          'shipping': '([{$order->oxorder__oxdelcost}]+[{$order->oxorder__oxpaycost}])',[{*Shipping.*}]
          'tax': '([{$order->oxorder__oxartvatprice1}]+[{$order->oxorder__oxartvatprice2}])'[{*Tax.*}]
        });
        [{* **********************************************************************************GA Basket******************************* *}]
        [{foreach from=$basket->getContents() item=basketitem}]
            [{assign var=basketproduct value=$basketitem->getArticle()}]
            [{assign var=dUnitPrice value=$basketitem->getUnitPrice()}]
            [{assign var=category value=$basketproduct->getCategory()}]
            ga('ecommerce:addItem', {
              'id': '[{$order->oxorder__oxordernr}]',[{*Transaction ID. Required.*}]
              'name': '[{$basketitem->getTitle()}]',[{*Product name. Required.*}]
              'sku': '[{$basketproduct->oxarticles__oxartnum->value}]',[{*SKU code.*}]
              'category': '[{$category->oxcategories__oxtitle->value}]',[{*Category or variation.*}]
              'price': '[{$dUnitPrice->getBruttoPrice()}]',[{*Unit price.*}]
              'quantity': '[{$basketitem->getAmount()}]'[{*Quantity.*}]
            });
        [{/foreach}]
        ga('ecommerce:send');
    [{/if}]
</script>
<!-- End Google Code -->