[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{if $oViewConf->getActiveClassName() == "thankyou"}]
    <!-- Google Adwords Conversion Tracking by https://eComStyle.de 4 OXID eShop 6 -->
    [{assign var="oConf" value=$oViewConf->getConfig()}]
    [{assign var="idadw" value=$oConf->getConfigParam('ecs_adwid')}]
    [{assign var="labeladw" value=$oConf->getConfigParam('ecs_adwlab')}]
    [{assign var="order" value=$oView->getOrder()}] 
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = [{$idadw}];
        var google_conversion_language = "de";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "[{$labeladw}]";
        var google_conversion_value = [{$order->oxorder__oxtotalordersum}];
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/[{$idadw}]/?value=[{$order->oxorder__oxtotalordersum}]&amp;label=[{$labeladw}]&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
    <!-- End Google Adwords Code -->
[{/if}]