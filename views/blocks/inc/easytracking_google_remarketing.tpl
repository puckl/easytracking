[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{assign var="oConf" value=$oViewConf->getConfig()}]
[{assign var="idrema" value=$oConf->getConfigParam('ecs_remaid')}]
[{assign var="labelrema" value=$oConf->getConfigParam('ecs_remalab')}]
<!-- Google Remarketing-Tag by https://eComStyle.de 4 OXID eShop 6 -->
[{*
<!--------------------------------------------------
Remarketing-Tags dürfen nicht mit personenbezogenen Daten verknüpft oder auf Seiten platziert werden, die sensiblen Kategorien angehören. Weitere Informationen und Anleitungen zur Einrichtung des Tags erhalten Sie unter: http://google.com/ads/remarketingsetup
--------------------------------------------------->
*}]
[{* Produktseiten *}]
[{if $oViewConf->getActiveClassName() == "details"}]
    [{assign var=pagetype value="product"}]
    [{capture name=detailsart}][{strip}]
        '[{$oDetailsProduct->oxarticles__oxartnum->value}]'
    [{/strip}][{/capture}]
    [{assign var=artnum value=$smarty.capture.detailsart}]
    [{assign var=artprice value=$oDetailsProduct->oxarticles__oxprice->value}]
[{* Kategorielisten *}]
[{elseif $oViewConf->getActiveClassName() == "alist"}]
    [{assign var=artnum value=false}]
    [{assign var=pagetype value="category"}]
[{* Startseite *}]
[{elseif $oViewConf->getActiveClassName() == "start"}]
    [{assign var=artnum value=false}]
    [{assign var=pagetype value="home"}]
[{* Suchergebnisse *}]
[{elseif $oViewConf->getActiveClassName() == "search"}]
    [{assign var=artnum value=false}]
    [{assign var=pagetype value="searchresults"}]
[{* Warenkorb ((Bestellschritt 1) *}]
[{elseif $oViewConf->getActiveClassName() == "basket" || $oViewConf->getActiveClassName() == "order"}]
    [{assign var=price value=$oxcmp_basket->getPrice()}]
    [{assign var=artprice value=$price->getBruttoPrice()}]
    [{assign var=pagetype value="cart"}]
    [{capture name=basketart}][{strip}][
        [{assign var=artnum value=$oDetailsProduct->oxarticles__oxartnum->value}]
        [{foreach key=basketindex from=$oxcmp_basket->getContents() item=basketitem name=artnr}]
            [{assign var=product value=$basketitem->getArticle()}]
            '[{$product->oxarticles__oxartnum->value}]'[{if !$smarty.foreach.artnr.last}], [{/if}]
        [{/foreach}]
    ][{/strip}][{/capture}]
    [{assign var=artnum value=$smarty.capture.basketart}]
[{* Thankyouseite ((Bestellschritt 5) *}]
[{elseif $oViewConf->getActiveClassName() == "thankyou"}]
    [{assign var="order" value=$oView->getOrder()}]
    [{assign var=artprice value=$order->oxorder__oxtotalordersum}]
    [{assign var=pagetype value="purchase"}]
    [{capture name=basketart}][{strip}][
        [{assign var=artnum value=$oDetailsProduct->oxarticles__oxartnum->value}]
            [{foreach from=$basket->getContents() item=basketitem name=artnr}]
                [{assign var=product value=$basketitem->getArticle()}]
                '[{$product->oxarticles__oxartnum->value}]'[{if !$smarty.foreach.artnr.last}], [{/if}]
            [{/foreach}]
        ][{/strip}][{/capture}]
        [{assign var=artnum value=$smarty.capture.basketart}]
[{* Alle anderen Seiten *}]
[{else}]
    [{assign var=artnum value=false}]
    [{assign var=pagetype value="other"}]
[{/if}]
<script type="text/javascript">
    var google_tag_params = {
    [{if $artnum}]ecomm_prodid: [{strip}][{$artnum}],[{/strip}][{/if}]
    ecomm_pagetype: '[{$pagetype}]',
    [{if $artprice}]ecomm_totalvalue: [{$artprice}][{/if}]
    };
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = [{$idrema}];
    [{if $labelrema}]var google_conversion_label = "[{$labelrema}]";[{/if}]
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/[{$idrema}]/?value=0&amp;label=[{$labelrema}]&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
<!-- End Google Remarketing-Tag -->