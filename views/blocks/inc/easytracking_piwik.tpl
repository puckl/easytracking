[{*
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */
 *}]
[{if !isset($oConfig)}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]
[{assign var="urlpiwikconf" value=$oConfig->getConfigParam('ecs_piwikurl')}]
[{assign var="urlpiwik"     value=$urlpiwikconf|replace:"http://":""|replace:"https://":""}]
[{assign var="idpiwik"      value=$oConfig->getConfigParam('ecs_piwikid')}]
[{assign var="piwikuserid"  value=$oConfig->getConfigParam('ecs_piwikuserid')}]
<!-- Matomo eCommerce Tracking by https://eComStyle.de 4 OXID eShop 6 -->
<script type="text/javascript">
var _paq = _paq || [];
[{* **********************************************************************************Kategorie******************************* *}]
[{if $oView->getClassName() == "alist"}][{strip}]
    [{*on a category page, productSKU and productName are not applicable and are set to false*}]
    _paq.push(['setEcommerceView',
    productSku = false,[{*No product on Category page*}]
    productName = false,[{*No product on Category page*}]
    category = "[{$oView->getTitle()}]"[{*Category Page, or array of up to 5 categories*}]
    ]);[{/strip}]
[{/if}]
[{* **********************************************************************************Artikeldetail******************************* *}]
[{if $oView->getClassName() == "details"}][{strip}]
    [{*all parameters are optional, but we recommend to set at minimum productSKU and productName*}]
    [{assign var=category value=$oDetailsProduct->getCategory()}]
    _paq.push(['setEcommerceView',
    "[{$oDetailsProduct->oxarticles__oxartnum->value}]",[{*(required) SKU: Product unique identifier*}]
    "[{$oDetailsProduct->oxarticles__oxtitle->value}]",[{*(optional) Product name*}]
    "[{$category->oxcategories__oxtitle->value}]",[{*(optional) Product category, or array of up to 5 categories*}]
    [{$oDetailsProduct->oxarticles__oxprice->value}][{*(optional) Product Price as displayed on the page*}]
    ]);[{/strip}]
[{/if}]
[{* *************************************************************************************Warenkorb**************************** *}]
[{if $oxcmp_basket->isNewItemAdded() || $oView->getClassName() == "basket" }]
    [{*add the first product to the order*}]
    [{foreach key=basketindex from=$oxcmp_basket->getContents() item=basketitem}][{strip}]
        [{assign var=product value=$basketitem->getArticle()}]
        [{assign var=itemprice value=$basketitem->getUnitPrice()}]
        [{assign var=category value=$product->getCategory()}]
        _paq.push(['addEcommerceItem',
        "[{$product->oxarticles__oxartnum->value}]",[{*(required) SKU: Product unique identifier*}]
        "[{$basketitem->getTitle()}]",[{*(optional) Product name*}]
        "[{$category->oxcategories__oxtitle->value}]",[{*(optional) Product category, string or array of up to 5 categories*}]
        [{$itemprice->getBruttoPrice()}],[{*(recommended) Product price*}]
        [{$basketitem->getAmount()}][{*(optional, default to 1) Product quantity *}]
        ]);[{/strip}]
    [{/foreach}][{strip}]
    [{*Records the cart for this visit*}]
    [{assign var=price value=$oxcmp_basket->getPrice()}]
    _paq.push(['trackEcommerceCartUpdate',
    [{$price->getBruttoPrice()}]]);[{*(recommended) Cart amount*}][{/strip}]
[{/if}]
[{* *************************************************************************************Thankyou**************************** *}]
[{if $oView->getClassName() == "thankyou"}]
    [{assign var="order" value=$oView->getOrder()}]
    [{*add the first product to the order*}]
    [{foreach from=$basket->getContents() item=basketitem}][{strip}]
        [{assign var=basketproduct value=$basketitem->getArticle()}]
        [{assign var=dUnitPrice value=$basketitem->getUnitPrice()}]
        [{assign var=category value=$basketproduct->getCategory()}]
        _paq.push(['addEcommerceItem',
        "[{$basketproduct->oxarticles__oxartnum->value}]",[{*(required) SKU: Product unique identifier*}]
        "[{$basketitem->getTitle()}]",[{*(optional) Product name*}]
        "[{$category->oxcategories__oxtitle->value}]",[{*(optional) Product category. You can also specify an array of up to 5 categories eg. ["Books", "New releases", "Biography"]*}]
        [{$dUnitPrice->getBruttoPrice()}],[{*(recommended) Product price*}]
        [{$basketitem->getAmount()}][{* (optional, default to 1) Product quantity*}]
        ]);[{/strip}]
    [{/foreach}][{strip}]
    [{*Specifiy the order details to Piwik server &amp; sends the data to Piwik server*}]
    _paq.push(['trackEcommerceOrder',
    "[{$order->oxorder__oxordernr}]",[{*(required) Unique Order ID *}]
    [{$order->oxorder__oxtotalordersum}],[{*(required) Order Revenue grand total (includes tax, shipping, and subtracted discount)*}]
    [{$order->oxorder__oxtotalbrutsum}],[{*(optional) Order sub total (excludes shipping)*}]
    ([{$order->oxorder__oxartvatprice1}]+[{$order->oxorder__oxartvatprice2}]),[{*(optional) Tax amount*}]
    ([{$order->oxorder__oxdelcost }]+[{$order->oxorder__oxpaycost }]),[{*(optional) Shipping amount*}]
    [{$order->oxorder__oxdiscount }][{*(optional) Discount offered (set to false for unspecified parameter)*}]
    ]);[{/strip}]
[{/if}]
[{* ************************************************************************************Ecommerce End***************************** *}]
[{if $oView->getClassName() == "search"}][{strip}]
_paq.push(['trackSiteSearch',
    "[{$smarty.get.searchparam|strip_tags}]",[{* // Search keyword searched for *}][{* // tabsl fix: strip->strip_tags *}]
    false,[{* // Search category selected in your search engine. If you do not need this, set to false *}]
    [{if $oView->getArticleCount() }][{$oView->getArticleCount() }][{else}]0[{/if}][{* // Number of results on the Search results page. Zero indicates a 'No Result Search Keyword'. Set to false if you don't know *}]
]);[{/strip}]
[{/if}]
[{* ************************************************************************************User ID***************************** *}]
[{if $piwikuserid && $oxcmp_user && $oxcmp_user->oxuser__oxcustnr->value}][{strip}]
_paq.push(['setUserId', '[{$oxcmp_user->oxuser__oxcustnr->value}]']);[{/strip}]
[{/if}]
[{* ************************************************************************************Start Standardpiwik***************************** *}]
[{if !$oConfig->getConfigParam('ecs_piwikcookie')}]
_paq.push(['disableCookies']);
[{/if}]
[{if $oView->getClassName() != "search"}]
_paq.push(['trackPageView']);
[{/if}]
_paq.push(['enableLinkTracking']);
(function() {
  var u=(("https:" == document.location.protocol) ? "https" : "http") + "://[{$urlpiwik}]/";
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setSiteId', [{$idpiwik}]]);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
  g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();
</script>
<noscript><p><img src="//[{$urlpiwik}]/piwik.php?idsite=[{$idpiwik}]&rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->